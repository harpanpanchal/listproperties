import React, { Component } from 'react';
import '../../src/style.scss';
import {connect} from 'react-redux';
import * as actionTypes from '../store/actions';
import { merge } from 'lodash';
class App extends Component {

render() {
let finalResult;
if (typeof(this.props.resultsData) !== 'undefined' || this.props.resultsData != null) {
    finalResult = this.props.resultsData.map(d => { return(
    
    <div key={d.id} className="card-deck text-center results">
        <div className="card"> 
        <div className="topHead" style={{backgroundColor: d.agency.brandingColors.primary}}><img className="card-img-top logo" src={d.agency.logo} alt="Card image cap" /></div>
          <div className="card-body"><img src={d.mainImage} />
          <div className="card-text priceInfo">{d.price}</div>
          <span><button onClick={this.props.onAddProperty.bind(this, d)}>Add</button></span>
          </div>
        </div></div>
            )}) 
    }
    let finalSavedData;
    if (typeof(this.props.savedData) !== 'undefined' || this.props.savedData != null) {
      finalSavedData = this.props.savedData.map(d => {return(<div key={d.id} className="card-deck text-center saved">
        <div className="card"> 
          <div className="topHead" style={{backgroundColor: d.agency.brandingColors.primary}}><img className="card-img-top logo" src={d.agency.logo} alt="Card image cap" /></div>
          <div className="card-body">
          <img src={d.mainImage} />
          <div className="card-text priceInfo">{d.price}</div>
                  <span><button onClick={this.props.onRemovedProperty.bind(this,d)}>Remove</button></span>
                  </div>
        </div></div>
            )}) 
      }
      if(finalSavedData == ""){
        finalSavedData = <p>No Record Found.</p>;
      }
     
      
   return (
      <div className="App">
      <div className="col-results">
      <h2>Results</h2>
      {finalResult}
      </div>
        <div className="col-saved">
        <h2>Saved Properties</h2>
        {finalSavedData}</div>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      resultsData: state.results,
      savedData: state.saved
  };
};

const mapDispatchToProps = dispatch => {
  return {
      onAddProperty: d => dispatch({type: actionTypes.ADD_PROPERTY, data: d}),
      onRemovedProperty: d => dispatch({type: actionTypes.REMOVE_PROPERTY, data:d})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);