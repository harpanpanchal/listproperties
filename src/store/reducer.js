import * as actionTypes from './actions';
import data from '../assets/data.json';

import { merge } from 'lodash';
const initialState = data;

const reducer = ( state = initialState, action ) => {

    switch ( action.type ) {

case actionTypes.ADD_PROPERTY:

const existRec = state.saved.filter(d => d.id === action.data.id);

if(Object.keys(existRec).length === 0) {
const addproperty = merge({}, state);
addproperty.saved.push(action.data);
return addproperty;
}
return state;

case actionTypes.REMOVE_PROPERTY:
const removeRec = state.saved.filter(d => d.id !== action.data.id);
return { results: state.results, saved: removeRec }
}
    return state;
};

export default reducer;