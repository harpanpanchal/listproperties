# listproperties

This project has been built using following front end tools.

1) React / Redux
2) Javascript
3) ES6
4) HTML5 / CSS3 / SASS / Bootstrap
5) Webpack / Babel

Steps to install the project are as follows.

1) Run 'npm install' command in the terminal to install all the required tools / dependencies.
2) And, then run 'npm start' command in the terminal to run the project. Check the terminal for the localhost url. For ex: http://localhost:8080/

:: Few additional features ::

1) Responsive website (Mobile / iPad / Desktop)
2) JSON data has been stored separately in data.json file within 'src/assets' folder. 
3) As mentioned above, Redux has been used to store the state at one place by using 'Actions', 'Reducer' created in separate files.

:: Scope of Improvements ::

 1) Separate components (for Result and Saved components) can be created and imported into App.js component. App.js.

 2) Folder structure also be created as well. For example, Components folder / Container folder. This way we could arrange the functional and class based components separately.

 3) Further validation can be set up on mousehover functionality - 'ADD' button to be visible only if the property has not been saved earlier. 

 4) Json data can be imported from the external API using Axios connecting inside React within Constructor function in the App.js file.

 5) Styling along with the other features like Footer / Top menu / Header can also be added depending upon the requirements.